import 'phaser';
import GamePlay from "./scenes/GamePlay";
import GameMenu from "./scenes/GameMenu";
import GameLevelUp from "./scenes/GameLevelUp";
import GameOver from "./scenes/GameOver";
import CONFIG from "../config";

let game : Phaser.Game;

window.onload = function() {
    let gameConfig = {
        type: Phaser.AUTO,
        backgroundColor: 0xB6D3FF,
        fps: {
            target: 60,
            forceSetTimeOut: true
        },
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_HORIZONTALLY,
            parent: "thegame",
            width: 760,
            height: 1372,
        },
        scene: [GamePlay, GameLevelUp, GameOver]
    }
    game = new Phaser.Game(gameConfig);
}
