import 'phaser';
import CONFIG from "../../config";

const WATING = 0;
const PLAY = 1;

// this class extends Scene class
var gameOptions = {

	// maximum length of the sum
	maxSumLen: 6,

	// local storage name used to save high score
	localStorageName: "oneplustwo",

	// time allowed to answer a question, in milliseconds
	timeToAnswer: 3000,  

	// score needed to increase difficulty
	nextLevel: 600
}

export default class GamePlay extends Phaser.Scene{
	isGameOver : boolean;
	score: number;
	correctAnswers : number;
	sumsArray : any;
	questionText: Phaser.GameObjects.Text;
	timeTween: any;
	buttonMask: Phaser.GameObjects.Sprite;
	randomSum: number;
	bestScore: any;
	bestScoreText: Phaser.GameObjects.BitmapText;
	scoreText: Phaser.GameObjects.BitmapText;
	logo: Phaser.GameObjects.Sprite;
	block_question: Phaser.GameObjects.Sprite;
	mascot_correct: Phaser.GameObjects.Sprite;
	mascot_wrong: Phaser.GameObjects.Sprite;
	gameState: number;
	playButton: Phaser.GameObjects.Sprite;
	currentTime: any;
	t0: any;

    constructor(){
        super("GamePlay");
		this.gameState = WATING;
    }

	// when the scene preloads...
    preload(){

		// preloading images
		this.load.image("timebar", "assets/sprites/timebar.png");

		// preloading a spritesheet where each sprite is 400x50 pixels
		this.load.spritesheet("buttons", "assets/sprites/buttons.png", {
            frameWidth: 400,
            frameHeight: 50
        });

		this.load.image("mascot_correct", "assets/sprites/mascot_correct.png");
		this.load.image("mascot_wrong", "assets/sprites/mascot_wrong.png");
		this.load.image("block", "assets/sprites/block.png");

		this.load.image("logo", "assets/sprites/banner.png");
        this.load.image("play", "assets/sprites/play.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel2.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels2.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");

    }

	// when the sceen has been created...
    create(){
		this.t0 = Date.now()/1000.;
		console.log(this.t0);
		
		// it's not game over yet...
		this.isGameOver = false;

		// current score is set to zero
		this.score = 0;

		// we'll also keep track of correct answers
		this.correctAnswers = 0;

		// topScore gets the previously saved value in local storage if any, zero otherwise
		this.bestScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

		// sumsArray is the array with all possible questions
		this.sumsArray = [];

		let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);

        this.mascot_correct = this.add.sprite(this.scale.width/2, this.scale.height/2-250, "mascot_correct");
		this.mascot_correct.setScale(0.6);
		this.mascot_correct.setOrigin(0.5, 1);
       
        this.block_question = this.add.sprite(this.scale.width/2, this.scale.height/2-100, "block");
		this.block_question.setScale(0.8, 0.7);
		this.block_question.setOrigin(0.5, 0.5);

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -30, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(0.9);

		// rather than tossing a random question each time, I found easier
		// to store all possible questions in an array then draw a random question
		// each time. I just need an algorithm to generate all possible questions.

		// let's start building all possible questions with this loop
		// ranging from 1 (only one operator, like 1+1) to maxSumLen
		// (in this case 5, like 1+1+1+1-1-1)
		for(var i = 1; i < gameOptions.maxSumLen; i++){

			// defining sumsArray[i] as an array of three empty arrays
			this.sumsArray[i]=[[], [], []];

			// looping from 1 to 3, which are the possible results of each sum
			for(var j = 1; j <= 3; j++){

				// buildTrees is the core of the script, see it explained
				// some lines below
				this.buildThrees(j, 1, i, j);
			}
		}

		// try this! You will see all possible combinations
		console.log(this.sumsArray);

		if(this.gameState == PLAY){
			// questionText is the text object which will display the question
			this.questionText = this.add.text(this.scale.width/2 , this.scale.height/2-75, "-", {
				font: "bold 90px Arial"
			});

			// setting questionText registration point to its center
			this.questionText.setOrigin(0.5);

			// loop to create the three answer buttons
			for(i = 0; i < 3; i++){

				// creation of the answer button
				var numberButton = this.add.sprite(this.scale.width / 2, this.scale.height/2 +150 + i * 75*1.8, "buttons");

				// showing the i-th frame
				numberButton.setFrame(i);

				// setting numberButton interactive
				numberButton.setInteractive();
				numberButton.setScale(1.8);

				// calling checkAnswer method if clicked/tapped
				numberButton.on("pointerdown", this.checkAnswer);
			}

			// adding the time bar
			var numberTimer =  this.add.sprite(this.scale.width / 2, this.scale.height/2 +150 + 75*1.8, "timebar");
			numberTimer.setScale(1.8);

			// the same image will also be used as a mask
			this.buttonMask = this.add.sprite(this.scale.width / 2, numberTimer.y, "timebar");
			this.buttonMask.setScale(1.8);

			// we do not need to show the mask image
			this.buttonMask.setVisible(false);

			// creation of the mask itself
			var mask = this.buttonMask.createBitmapMask();

			// applying mask to number timer
			numberTimer.setMask(mask);

			// method to ask next question
			this.nextNumber();
			this.playButton.setVisible(false);
		}
		else{
			var text = this.add.text(this.scale.width/2 , this.scale.height/4+180, "Welcome to", {
				font: "italic 60px Arial"
			});
			text.setOrigin(0.5, 0.5);
            var text = this.add.text(this.scale.width/2 , this.scale.height/2-75, "1+2+3 Game", {
				font: "bold 70px Arial"
			});
			text.setOrigin(0.5, 0.5);

			this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2+290, "play");
			this.playButton.setScale(1)
			this.playButton.setOrigin(0.5,0.5);
			this.playButton.setInteractive();
			this.playButton.on("pointerdown", function(){
				this.scene.start("GamePlay");
				this.gameState = PLAY;
			}, this)
		}
		this.game.events.addListener(Phaser.Core.Events.FOCUS, this.focus, this);
	}

	focus(){
		if(this.score > 0){
			this.gameOver("Out focus");
		}
		console.log("Focus has gained");
	}

	// buildThrees method, it will find all possible sums
	// arguments:
	// initialNumber: the first number. Each question always start with a positive number
	// currentIndex: it's the amount of operands already placed in the sum
	// limit: the max amount of operands allowed in the question
	// currentString: the string generated so far
	buildThrees(initialNummber, currentIndex, limit, currentString){

		// the possible operands, from -3 to 3, excluding the zero
		var numbersArray = [-3, -2, -1, 1, 2, 3];

		// looping from 0 to numbersArray's length
		for(var i = 0; i < numbersArray.length; i++){

			// "sum" is the sum between the first number and current numberArray item
			var sum = initialNummber + numbersArray[i];

			// output string is generated by the concatenation of current string with
			// current numbersArray item. I am adding a "+" if the item is greater than zero,
			// otherwise it already has its "-"
			var outputString = currentString + (numbersArray[i] < 0 ? "" : "+") + numbersArray[i];

			// if sum is between 1 and 3 and we reached the limit of operands we want...
			if(sum > 0 && sum < 4 && currentIndex == limit){

				// then push the output string into sumsArray[amount of operands][result]
				this.sumsArray[limit][sum - 1].push(outputString);
			}

			// if the amount of operands is still below the amount we want...
			if(currentIndex < limit){

				// recursively calling buildThrees, passing as arguments:
				// the current sum
				// the new amount of operands
				// the amount of operands we want
				// the current output string
				this.buildThrees(sum, currentIndex + 1, limit, outputString);
			}
		}
	}

	// this method asks next question
	nextNumber(){

		// updating score text
		// this.scoreText.setText("Score: " + this.score.toString() + "\nBest Score: " + this.bestScore.toString());
		this.t0 = Date.now()/1000.;
		// if we already answered more than one question...
		if(this.correctAnswers > 1){

			// stopping time tween
			this.timeTween.stop();

			// resetting mask horizontal position
			this.buttonMask.x = this.scale.width / 2;
		}

		// if we already answered at least one question...
		if(this.correctAnswers > 0){

			// tween to slide out the mask, unvealing what's behind it
            this.timeTween = this.tweens.add({
                targets: this.buttonMask,
                x: -350,
                duration: gameOptions.timeToAnswer,
                callbackScope: this,
                onComplete: function(){

                    // calling "gameOver" method. "?" is the string to display
                    this.gameOver("?");
                }
            });
		}

		// drawing a random result between 0 and 2 (it will be from 1 to 3)
		this.randomSum = Phaser.Math.Between(0, 2);

		// choosing question length according to current score
		var questionLength = Math.min(Math.floor(this.score / gameOptions.nextLevel) + 1, 4)

		// updating question text
		this.questionText.setText(this.sumsArray[questionLength][this.randomSum][Phaser.Math.Between(0, this.sumsArray[questionLength][this.randomSum].length - 1)] + " = ?");
	}

	// method to check the answer, the argument is the button pressed
	checkAnswer(){

		let v = this.scene as any;
		let tt = this as any;
		let frameIdx = tt.frame.name;
		// we check the answer only if it's not game over yet
		if(!v.isGameOver){

			// button frame is equal to randomSum means the answer is correct
			if(frameIdx == v.randomSum){

				// score is increased according to the time spent to answer
     			v.score += Math.floor((v.buttonMask.x + 350) / 4);
				 if(v.bestScore < v.score)
				 {
					 v.bestScore = v.score;
					 v.bestScoreText.text = v.bestScore.toString();
				 }
				 v.scoreText.text = v.score.toString();

				let t1 = Date.now()/1000.;
				let diff = t1 - v.t0;
				console.log(v.t0, t1, diff);
				if(diff > 3.5 && v.score > 200){
					v.gameOver("Out focus");
				}

				// one more correct answer
				v.correctAnswers++;

				// moving on to next question
				v.nextNumber();

     		}

			// wrong answer
     		else{

				// if it's not the first question...
     			if(v.correctAnswers > 1) {

					// stop the tween
					v.timeTween.stop();
     			}

				// calling "gameOver" method. "this.frame.name + 1" is the string to display
     			v.gameOver((frameIdx + 1).toString());
			}
		}
	}

	// method to end the game. The argument is the string to write
	gameOver(gameOverString){

		// changing background color
        // this.cameras.main.setBackgroundColor("#ff0000");
		this.cameras.main.shake(800, 0.01);

		this.mascot_correct.destroy();
		this.mascot_wrong = this.add.sprite(this.scale.width/2, this.scale.height/2-250, "mascot_wrong");
		this.mascot_wrong.setScale(0.6);
		this.mascot_wrong.setOrigin(0.5, 1);
		
		this.block_question.destroy();
        this.block_question = this.add.sprite(this.scale.width/2, this.scale.height/2-100, "block");
		this.block_question.setScale(0.8, 0.7);
		this.block_question.setOrigin(0.5, 0.5);

		// displaying game over text
		var text = this.add.text(this.scale.width/2 , this.scale.height/2-190, "Seriously ?!", {
			font: "bold 40px Arial"
		});
		text.setOrigin(0.5,0.5);

		var s = this.questionText.text;
		var newstr = s.replace("?", gameOverString);

		this.questionText.destroy();
		this.questionText = this.add.text(this.scale.width/2 , this.scale.height/2-75, newstr, {
			font: "bold 90px Arial"
		});
		this.questionText.setOrigin(0.5,0.5);

		// now it's game over
		this.isGameOver = true;

		// updating top score in local storage
		localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());

		this.playButton.destroy();
		this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2+290, "play");
		this.playButton.setScale(1)
		this.playButton.setOrigin(0.5,0.5);
		this.playButton.setInteractive();
		this.playButton.on("pointerdown", function(){
			this.scene.start("GamePlay");
			this.gameState = PLAY;
		}, this)
	}
}
